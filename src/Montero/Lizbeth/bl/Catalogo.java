package Montero.Lizbeth.bl;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase Catalogo, la cual es creada en el en la package o capa bl y aquí es en donde se guardan los valores de cada atributo del catálogo.
 *
 * @author Lizbeth Montero Ramirez.
 * @version 1.0.1
 * @since 1.0.0
 */
public class Catalogo {
    //Atributos
    private String Id;
    private String nombreDelMeS;
    private LocalDate fechaDeCreacion;
    private ArrayList<Camisa> camisas;
    private ArrayList<Cliente> clientes;

    /**
     * constructor por defecto de la clase catálogo que es utilizado para la invocación de las subclases.
     */
    public Catalogo() {
    }// FIN DE CONSTRUCTOR POR DEFECTO.

    /**
     * Constructor que recibe todos los parámetros, evita que algún valor se almacene por defecto osea con el valor de "null" en Strings o "0" en enteros..
     * @param id:              dato identificativo del catálogo, hace referencia a un codigo único del mismo.
     * @param fechaDeCreacion: dato referente a la fecha de creación del catálogo ingresado por el usuario; es de tipo String.
     * @param nombreDelMeS:    variable en donde se almacena el nombre del mes de creación de dicho catálogo.
     */

    public Catalogo(String id, String nombreDelMeS,  LocalDate fechaDeCreacion) {
        Id = id;
        this.nombreDelMeS = nombreDelMeS;
        this.fechaDeCreacion = fechaDeCreacion;
        this.clientes= new ArrayList<>();
        this.camisas = new  ArrayList<> ();
    }

    /**
     * Metodo Gettter de la variable que almacena el código identificativo del catalogo; es utilizado para obterner el atributo privado de una clase.
     *
     * @return retorna del atributo de que almacena Id del catálogo, ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getId() {
        return Id;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase, en este caso al código del catálogo.
     *
     * @param id: variable que almacena el codigo del catalogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setId(String id) {
        Id = id;
    }


    /**
     * Metodo get de la variable que guarda el el nombre del mes en el que fue creado el catálogo. Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la variable que guarda el nombre del mes de creación del catálogo el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */

    public String getNombreDelMeS() {
        return nombreDelMeS;
    }

    /**
     * Metodo setes utilizado para asignar valores a los atributos privados de una clase, en este caso a la variable de la fecha de creación del catálogo.
     *
     * @param nombreDelMeS: variable que guarda el nombre del mes de la creación del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */

    public void setNombreDelMeS(String nombreDelMeS) {
        this.nombreDelMeS = nombreDelMeS;
    }
    /**
     * Metodo get de la variable que guarda los clientes del catálogo. Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve los clientes guardados en el catálogo el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }
    /**
     * Metodo seter utilizado para asignar valores a los atributos privados de una clase, en este caso a la variable de la fecha de creación del catálogo.
     *
     * @param clientes: variable que guarda el nombre del mes de la creacion del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }
    /**
     * Metodo get de la variable que guarda los las camisas del catalogo. Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve las camisas guardados en el catálogo el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public ArrayList<Camisa> getCamisas() {
        return camisas;
    }
    /**
     * Metodo seter utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param camisas: variable que guarda el nombre del mes de la creacion del catálogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCamisas(ArrayList<Camisa> camisas) {
        this.camisas = camisas;
    }

    /**
     * metod utilizado para agregar la camisa al catalogolo
     * @param camisa tipo camisa, almacena una camisa ya creada.
     */
    public void agregarCamisa(Camisa camisa) {
        camisas.add(camisa);
    }

    /**
     * metdo que agrega el cliente
     * @param cliente tipo cliente, agrega el cliente
     */
    public void agregarCliente(Cliente cliente) {
        clientes.add(cliente);

    }

    /**
     * Metodo get de la variable que guarda la fecha de creacion  del catálogo. Se usa para obtener información del atributo privado de un objeto en su respectiva  clase.
     *
     * @return devuelve la fecha de creacion  catalogo el cual es ingresado por el usario en la capa ui y se almacena en su respectiva uclase en la capa bl.
     */
    public LocalDate getFechaDeCreacion() {
        return fechaDeCreacion;
    }
    /**
     * Metodo seter utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaDeCreacion: variable que guarda la fecha de creacion del catalogo ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setFechaDeCreacion(LocalDate fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo del catálogo en un solo String.
     */
    @Override
    public String toString() {
        return "Catalogo{" +
                "Id='" + Id + '\'' +
                ", nombreDelMeS='" + nombreDelMeS + '\'' +
                ", fechaDeCreacion=" + fechaDeCreacion +
                ", camisas=" + camisas +
                ", clientes=" + clientes +
                '}';
    }
}// FIN DE PROGRAMA.